package com.t3h.news.service.impl;

import com.t3h.news.dao.ICategoryDao;
import com.t3h.news.dao.repositorySpringDataJpa.INewsRepository;
import com.t3h.news.model.NewsModel;
import com.t3h.news.model.entity.CategoryEntity;
import com.t3h.news.model.entity.NewsEntity;
import com.t3h.news.model.request.NewsRequest;
import com.t3h.news.service.IFileService;
import com.t3h.news.service.INewsService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.io.File;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NewsServiceImpl implements INewsService {

    private final INewsRepository repository;
    private final ModelMapper modelMapper;
    private final ICategoryDao iCategoryDao;
    private final IFileService iFileService;

    public NewsServiceImpl(INewsRepository repository, ModelMapper modelMapper, ICategoryDao iCategoryDao, IFileService iFileService) {
        this.repository = repository;
        this.modelMapper = modelMapper;
        this.iCategoryDao = iCategoryDao;
        this.iFileService = iFileService;
    }


    @Override
    public List<NewsModel> getList() {
        return repository.findAll().stream().map(entity -> modelMapper.map(entity,NewsModel.class)).collect(Collectors.toList());
    }

    @Override
    public NewsModel add(NewsRequest newsRequest) {
        newsRequest.setCreateDate(new Timestamp(System.currentTimeMillis()));
        newsRequest.setUpdateDate(new Timestamp(System.currentTimeMillis()));
        newsRequest.setNumberAccess(0);
        newsRequest.setCreatorId(1);
        newsRequest.setEditorId(1);
        newsRequest.setCensor(1);

        NewsEntity newsEntity = modelMapper.map(newsRequest,NewsEntity.class);

        CategoryEntity categoryEntity = iCategoryDao.findById(newsRequest.getCategoryId());
        newsEntity.setCategory(categoryEntity);

        String pathFile = "C:\\Users\\Admin\\Desktop\\out\\24h\\src\\main\\resources\\static\\admin\\img\\" + "news\\";
        File fileSaved = iFileService.uploadFile(newsRequest.getAvatar(), pathFile);

        String pathAvatar = "/admin/img/" + "news/" + fileSaved.getName();

        File targetAvatar = new File("C:\\Users\\Admin\\Desktop\\out\\24h\\target\\classes\\static\\admin\\img\\news\\" + fileSaved.getName());
        iFileService.copyFile(fileSaved,targetAvatar);
        newsEntity.setAvatar(pathAvatar);
        newsEntity = repository.save(newsEntity);

        return null;
    }

    @Override
    public NewsModel findById(Integer id) {
        NewsEntity newsEntity = repository.findById(id).orElse(new NewsEntity());
        NewsModel newsModel = modelMapper.map(newsEntity,NewsModel.class);
        newsModel.setCategoryName(newsEntity.getCategory().getName());
        return newsModel;
    }
}
