package com.t3h.news.service;

import com.t3h.news.model.CategoryModel;
import com.t3h.news.model.response.SystemResponse;

import java.util.List;

public interface ICategoryService {

    SystemResponse<List<CategoryModel>> getAll();

    void add(CategoryModel categoryModel);

    SystemResponse<CategoryModel> findById(int id);
}
