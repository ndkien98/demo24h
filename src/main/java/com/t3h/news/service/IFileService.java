package com.t3h.news.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public interface IFileService {

    File uploadFile(MultipartFile file,String path);

    void copyFile(File source, File target);
}
