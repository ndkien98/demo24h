package com.t3h.news.service;

import com.t3h.news.model.NewsModel;
import com.t3h.news.model.request.NewsRequest;

import java.util.List;

public interface INewsService {

    List<NewsModel> getList();

    NewsModel add(NewsRequest newsRequest);

    NewsModel findById(Integer id);
}
