package com.t3h.news.dao.repositorySpringDataJpa;

import com.t3h.news.model.entity.NewsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface INewsRepository extends JpaRepository<NewsEntity,Integer> {
}
