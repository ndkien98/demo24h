package com.t3h.news.controller.api;

import com.t3h.news.model.CategoryModel;
import com.t3h.news.model.response.SystemResponse;
import com.t3h.news.service.ICategoryService;
import com.t3h.news.utils.Constant;
import com.t3h.news.utils.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  RestService -> json
 *  soap -> xml
 *
 */
@RestController
@RequestMapping("api/admin/category")
public class CategoryApiController {


    private final ICategoryService iCategoryService;


    public CategoryApiController(ICategoryService iCategoryService) {
        this.iCategoryService = iCategoryService;
    }

    @GetMapping()
    public ResponseEntity<SystemResponse<List<CategoryModel>>> getAll(){
        return ResponseEntity.ok(iCategoryService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<SystemResponse<CategoryModel>> findById(@PathVariable int id){
        return ResponseEntity.ok(iCategoryService.findById(id));
    }

    @PostMapping()
    public ResponseEntity<SystemResponse> add(@RequestBody CategoryModel categoryModel){
        iCategoryService.add(categoryModel);
        return ResponseEntity.ok(new SystemResponse(HttpStatusCode.OK, Constant.SUCCESS,null));
    }
}
